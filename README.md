# ChessBoard

It was my first longer java project. <br>
I had to implement a chessboard. I've generated the steps randomly and I've built some game logic in:  <br><br>
-the elements can "see" the table so they will take a step only when it is possible <br>
-they can kill each other, if an element is killed, it is removed from the table<br>
-the game terminates when either the black or white king is dead <br>
-each element has a code, the white king has code 71 and the black king has code 79. So if the white one wins, the number 79 won't appear in the last output matrix <br>
-the output shows the position of the elements in every step <br>