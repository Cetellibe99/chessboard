import java.util.*;


 class RookBlack extends Figure {
	int index,x,y;
	String color;
	Boolean isDead;
	
	RookBlack(int index,int x,int y, String color, Boolean isDead){
		this.index = index;
		this.x=x;
		this.y=y;
		this.color = color;
		this.isDead=isDead;
	
	}
	Boolean canmove(Integer [][] matrix) { 
		
		Integer zero = 0;
		
		//if (((this.x >=0) && (this.y>=0))&& ((this.x<8) && (this.y <8))) { //check for out of range
		if (isInRange(x+1))
			if (isInRange(y))
				if (matrix[x+1][y].equals(zero)) //check for already occupied pos
					return true;
		
		if (isInRange(x-1))
			if (isInRange(y))
				if (matrix[x-1][y].equals(zero))
					return true;
			
		if (isInRange(x))
			if (isInRange(y+1))
				if (matrix[x][y+1].equals(zero))
					return true;
			
		if (isInRange(x))
			if (isInRange(y-1))
				if (matrix[x][y-1].equals(zero))
					return true;
			
			return false;
			
	
		
	
	}
	
	void move (Integer [][] matrix, int [][]visual, int nrofsteps, Integer direction) {
		if (direction.equals(1)) {
			this.x  = this.x+nrofsteps;
			//System.out.println("x of rook bekame    " + this.x);
			//System.out.println("x of rook bekame    " + x);
			matrix[x-nrofsteps][y]=0;
			matrix[x][y] = 1;
			visual[x-nrofsteps][y]=0;
			visual[x][y] = this.index;
			
			
			
			
		}
		if (direction.equals(2)) {
			this.x  = this.x-nrofsteps;
			matrix[x+nrofsteps][y]=0;
			matrix[x][y] = 1;
			visual[x+nrofsteps][y]=0;
			visual[x][y] = this.index;
		}
		if (direction.equals(3)) {
			this.y  = this.y+nrofsteps;
			matrix[x][y-nrofsteps]=0;
			matrix[x][y] = 1;
			visual[x][y-nrofsteps]=0;
			visual[x][y] = this.index;
		}
		if (direction.equals(4)) {
			this.y  = this.y-nrofsteps;
			matrix[x][y+nrofsteps]=0;
			matrix[x][y] = 1;
			visual[x][y+nrofsteps]=0;
			visual[x][y] = this.index;
		}
		
		
	}
	int [] checkforenemies(int [][] visual) { //returns the closest figure, nincs direction,csak elore nezi :(
		int [] coord = new int[2];
		for (int i=1; i<=this.x; i++) { // !!!! ATIRNI MAJD MINDENHOL
			//System.out.println("the index i = : " +i);
			if (visual[this.x-i][this.y]!=0) {
				//System.out.println("enemy found at: " +(this.x-i)+ ""+this.y);
				if(isBlack(visual[this.x-i][this.y])) {
					coord[0]=-1;
					coord[1]=-1; //white in front
					return coord;
				}
				else
				{
					coord[0] = this.x-i; //black in front, returns the coordinates of the enemies
					coord[1] = this.y;
					return coord;
				}
				
				
			}
			
		}
		coord[0]=-2;
		coord[1]=-2; //blank space
		return coord; //blank space so far
	}
	
	
	

}
