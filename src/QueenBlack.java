


class QueenBlack extends Figure {

	int index,x,y;
	String color;
	Boolean isDead;
	
	QueenBlack(int index,int x,int y, String color, Boolean isDead){
		this.index = index;
		this.x=x;
		this.y=y;
		this.color = color;
		this.isDead = isDead;
	}
	
	Boolean canmove (Integer[][] matrix) { 
		Integer zero = 0;//checks if it can do any kind of step
		if (isInRange(x+1))
			if (isInRange(y+1))
				if (matrix[this.x+1][this.y+1].equals(zero) ) 
					return true;
		
		if (isInRange(x-1))
			if (isInRange(y+1))
				if (matrix[this.x-1][this.y+1].equals(zero) ) 
					return true;
		
		if (isInRange(x+1))
			if (isInRange(y-1))
				if (matrix[this.x+1][this.y-1].equals(zero) ) 
					return true;
		
		if (isInRange(x-1))
			if (isInRange(y-1))
				if (matrix[this.x-1][this.y-1].equals(zero) ) 
					return true;
		
		if (isInRange(x+1))
			if (isInRange(y))
				if (matrix[this.x+1][this.y].equals(zero) ) 
					return true;
		
		if (isInRange(x-1))
			if (isInRange(y))
				if (matrix[this.x-1][this.y].equals(zero) ) 
					return true;
		
		if (isInRange(x))
			if (isInRange(y-1))
				if (matrix[this.x][this.y-1].equals(zero) ) 
					return true;
		
		if (isInRange(x))
			if (isInRange(y+1))
				if (matrix[this.x][this.y+1].equals(zero) ) 
					return true;
		return false;
	}
	
	void move (Integer [][] matrix, int [][]visual, int spre, Integer direction) {
		if (direction.equals(1)) {
			this.x  = this.x+spre;
			this.y = this.y +spre;
			matrix[x-spre][y-spre]=0;
			matrix[x][y] = 1;
			visual[x-spre][y-spre]=0;
			visual[x][y] = this.index;
		}
		if (direction.equals(2)) {
			this.x  = this.x-spre;
			this.y = this.y +spre;
			matrix[x+spre][y-spre]=0;
			matrix[x][y] = 1;
			visual[x+spre][y-spre]=0;
			visual[x][y] = this.index;
		}
		if (direction.equals(3)) {
			this.x  = this.x+spre;
			this.y = this.y -spre;
			matrix[x-spre][y+spre]=0;
			matrix[x][y] = 1;
			visual[x-spre][y+spre]=0;
			visual[x][y] = this.index;
		}
		if (direction.equals(4)) {
			this.x  = this.x-spre;
			this.y = this.y -spre;
			matrix[x+spre][y+spre]=0;
			matrix[x][y] = 1;
			visual[x+spre][y+spre]=0;
			visual[x][y] = this.index;
		}
		if (direction.equals(5)) {
			//this.x  = this.x;
			this.y = this.y -spre;
			matrix[x][y+spre]=0;
			matrix[x][y] = 1;
			visual[x][y+spre]=0;
			visual[x][y] = this.index;
		}
		if (direction.equals(6)) {
			//this.x  = this.x-spre;
			this.y = this.y +spre;
			matrix[x][y-spre]=0;
			matrix[x][y] = 1;
			visual[x][y-spre]=0;
			visual[x][y] = this.index;
		}
		if (direction.equals(7)) {
			this.x  = this.x-spre;
			//this.y = this.y -spre;
			matrix[x+spre][y]=0;
			matrix[x][y] = 1;
			visual[x+spre][y]=0;
			visual[x][y] = this.index;
		}
		if (direction.equals(8)) {
			this.x  = this.x+spre;
			//this.y = this.y -spre;
			matrix[x-spre][y]=0;
			matrix[x][y] = 1;
			visual[x-spre][y]=0;
			visual[x][y] = this.index;
		}
	}
	
	
	int [] checkforenemies(int [][] visual, int sprex, int sprey) {
		int [] coord = new int[2];
		if (isInRange(this.x+sprex)) {
			if(isInRange(this.y+sprey))
			{
				if (visual[this.x+sprex][this.y+sprey]!=0) {
					if(isBlack(visual[this.x+sprex][this.y+sprey])) {
						coord[0]=-1;
						coord[1]=-1; //white in front
					}
					else
					{
						coord[0] = this.x+sprex; //black in front, returns the coordinates of the enemies
						coord[1] = this.y+sprex;
					}
					return coord;
					
				}
				else
				{
					coord[0]=-2;
					coord[1]=-2; //blank space
					return coord;
				}

			}
			else {
				coord[0]=-3; //OUT OF RANGE
				coord[1]=-3;
				return coord;
			}
		}
			else {
				coord[0]=-3; //OUT OF RANGE
				coord[1]=-3;
				return coord;
			}
		}
	
	int [] checkforenemiesQueenBlack(int [][] visual) {
		int [] coordi = new int[2];
		for (int i=1; i<8; ++i) {
			coordi = checkforenemies(visual,i,i);
			if (coordi[0]==(this.x+i))
				if(coordi[1]==(this.y+i))
					return coordi; //enemy found
			coordi = checkforenemies(visual,i,-i);
			if (coordi[0]==(this.x+i))
				if(coordi[1]==(this.y-i))
					return coordi; //enemy found
			coordi = checkforenemies(visual,-i,i);
			if (coordi[0]==(this.x-i))
				if(coordi[1]==(this.y+i))
					return coordi; //enemy found
			coordi = checkforenemies(visual,-i,-i);
			if (coordi[0]==(this.x-i))
				if(coordi[1]==(this.y-i))
					return coordi; //enemy found
			
			if (coordi[0]==(this.x+i))
				if(coordi[1]==(this.y))
					return coordi; //enemy found
			if (coordi[0]==(this.x-i))
				if(coordi[1]==(this.y))
					return coordi; //enemy found
			if (coordi[0]==(this.x))
				if(coordi[1]==(this.y+i))
					return coordi; //enemy found
			if (coordi[0]==(this.x))
				if(coordi[1]==(this.y-i))
					return coordi; //enemy found
			
			
			
			coordi[0]=-2; //if no enemies, there should be a side with blank space probablu
			coordi[1]=-2;
		
		}
		return coordi;
			
	}
}
