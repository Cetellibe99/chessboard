

class BishopBlack extends Figure {
	 
		int index,x,y;
		String color;
		Boolean isDead;
		
		BishopBlack(int index,int x,int y, String color, Boolean isDead){
			this.index = index;
			this.x=x;
			this.y=y;
			this.color = color;
			this.isDead = isDead;
		}
		
		Boolean canmove (Integer[][] matrix) { 
			Integer zero = 0;//checks if it can do any kind of step
			if (isInRange(x+1))
				if (isInRange(y+1))
					if (matrix[this.x+1][this.y+1].equals(zero) ) 
						return true;
			
			if (isInRange(x-1))
				if (isInRange(y+1))
					if (matrix[this.x-1][this.y+1].equals(zero) ) 
						return true;
			
			if (isInRange(x+1))
				if (isInRange(y-1))
					if (matrix[this.x+1][this.y-1].equals(zero) ) 
						return true;
			
			if (isInRange(x-1))
				if (isInRange(y-1))
					if (matrix[this.x-1][this.y-1].equals(zero) ) 
						return true;
			return false;
		}
		void move (Integer [][] matrix, int [][]visual, int spre, Integer direction) {
			if (direction.equals(1)) {
				this.x  = this.x+spre;
				this.y = this.y +spre;
				matrix[x-spre][y-spre]=0;
				matrix[x][y] = 1;
				visual[x-spre][y-spre]=0;
				visual[x][y] = this.index;
			}
			if (direction.equals(2)) {
				this.x  = this.x-spre;
				this.y = this.y +spre;
				matrix[x+spre][y-spre]=0;
				matrix[x][y] = 1;
				visual[x+spre][y-spre]=0;
				visual[x][y] = this.index;
			}
			if (direction.equals(3)) {
				this.x  = this.x+spre;
				this.y = this.y -spre;
				matrix[x-spre][y+spre]=0;
				matrix[x][y] = 1;
				visual[x-spre][y+spre]=0;
				visual[x][y] = this.index;
			}
			if (direction.equals(4)) {
				this.x  = this.x-spre;
				this.y = this.y -spre;
				matrix[x+spre][y+spre]=0;
				matrix[x][y] = 1;
				visual[x+spre][y+spre]=0;
				visual[x][y] = this.index;
			}
		}
		
		int [] checkforenemies(int [][] visual, int sprex, int sprey) {
			int [] coord = new int[2];
			coord[0]=-1;
			coord[1]=-1;
			if (isInRange(this.x+sprex)) {
				if(isInRange(this.y+sprey))
				{
					if (visual[this.x+sprex][this.y+sprey]!=0) {
						//System.out.println("visual kords: " + (this.x+sprex) + "  " + (this.y+sprey)+ "  ");
						if(isBlack(visual[this.x+sprex][this.y+sprey])) {
							coord[0]=-1;
							coord[1]=-1; //white in front
						}
						else
						{
							coord[0] = this.x+sprex; //black in front, returns the coordinates of the enemies
							coord[1] = this.y+sprex;
							System.out.println("");
							System.out.println("visual kords22: " + (this.x+sprex) + "  " + (this.y+sprey)+ "  ");
							
							return coord;
						}
						//return coord;
						
					}
					else
					{
						coord[0]=-2;
						coord[1]=-2; //blank space
						//return coord;
					}

				}
				else {
					coord[0]=-3; //OUT OF RANGE
					coord[1]=-3;
					//return coord;
				}
			}
				else {
					coord[0]=-3; //OUT OF RANGE
					coord[1]=-3;
					//return coord;
				}
		//	System.out.println(coord[0] + " masik koord: " + coord[1]);
			return coord;
			}
		
		//6,2 : for i:
		int [] checkforenemiesBishopBlack(int [][] visual) {
			int [] coordi = new int[2];
			for (int i=1; i<8; ++i) {
				
				coordi = checkforenemies(visual,i,i);
		
				if (coordi[0]==(this.x+i)) //ha visszateritette az ellenseg koordinatait
					if(coordi[1]==(this.y+i))
						return coordi; //enemy found
				
			
				coordi = checkforenemies(visual,i,-i);
				System.out.println("i= " +i);
				System.out.println("x,yok : " +(this.x+i)+" "+(this.y+i));
				System.out.println("coordok : " +(coordi[0])+(coordi[1]));
				if (coordi[0]==(this.x+2))
					if(coordi[1]==(this.y-2))
						return coordi; //enemy found
				
				coordi = checkforenemies(visual,-i,i);
				if (coordi[0]==(this.x-i))
					if(coordi[1]==(this.y+i))
						return coordi; //enemy found
				
				coordi = checkforenemies(visual,-i,-i);
				if (coordi[0]==(this.x-i))
					if(coordi[1]==(this.y-i))
						return coordi; //enemy found
				
		
			
			}
			coordi[0]=-2; //if no enemies, there should be a side with blank space probablu
			coordi[1]=-2;
			return coordi;		
			
		}
}
