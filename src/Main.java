import java.io.*;
import java.util.*;
public class Main extends SharePoint {
	
	 //x: rows in Integer matrix
	//y: columns in Integeer matrix
	
static Boolean isWhite(int index) {
		
		if (index >0) {
			if ((index-10)<10) //white pawn
				return true;
			if ((index >20) && (index <30) )
				return false; //black pawn
			if (index%10 <3)
				return true; //white but not pawn
		}
	
		
		return false; //black and not pawn or blank
			
	}
static	Boolean isBlack(int index) {
	
		if (index ==0)
			return false;
		
		if (index >0) {
			if ((index-10)<10) //white pawn
				return false;
			if ((index >20) && (index <30) )
				return true; //black pawn
			if (index%10 <3)
				return false; //white but not pawn
		}
	
		
		return true; //black and not pawn or blank
			
	}
	
	static Boolean  isInRange(Integer x) {
		if (x>=0 )
			if (x<8)
				return true;
		return false;
	}
	
	static void printVisual (int [][]visual) {
		System.out.println("");
		System.out.println("");
		System.out.println("visually : ");
		for (int i=0; i<8; ++i) {
			for (int j=0; j<8; ++j) {
				System.out.print(visual[i][j]+ " ");
				
			}
			System.out.println("");
		}
		
	}
	static void printBoolMatrix (Integer [][]matrix) {
		System.out.println("");
		System.out.println("");
		System.out.println("bool matrix : ");
		for (int i=0; i<8; ++i) {
			for (int j=0; j<8; ++j) {
				System.out.print(matrix[i][j]+ " ");
				
			}
			System.out.println("");
			
		}
		
	}
	static Boolean isOnTable(int [][] visual, int index) {
		
		for (int i=0; i<8; ++i) {
			for (int j=0; j<8; ++j) {
				if (visual[i][j]==index)
					return true;
			}
		}
		return false;
		
	}


	public static void main(String[] args) {
		
		Integer [][] matrix = new Integer[][] 
				{{1,1,1,1,1,1,1,1},  //whites up, blacks down
				{1,1,1,1,1,1,1,1},
				{0,0,0,0,0,0,0,0},
				{0,0,0,0,0,0,0,0},
				{0,0,0,0,0,0,0,0},
				{0,0,0,0,0,0,0,0},
				{1,1,1,1,1,1,1,1},
				{1,1,1,1,1,1,1,1}};
				
			int [][] visual = new int[][]
			   {{31,41,51,61,71,52,42,32}, //61 is queen
			   {11,12,13,14,15,16,17,18}, //white pawns
			   {0,0,0,0,0,0,0,0},
				{0,0,0,0,0,0,0,0},
				{0,0,0,0,0,0,0,0},
				{0,0,0,0,0,0,0,0},
				{21,22,23,24,25,26,27,28}, //black pawns
				{38,48,58,69,79,59,49,39}
				
				};
				
				Random rand = new Random();	
			
			
			//INITIALIZATION
				
			
			Pawn []whitepawns = new Pawn[8];
			whitepawns[0] = new Pawn(11,1,0, "white", false);
			whitepawns[1] = new Pawn(12,1,1, "white", false);
			whitepawns[2] = new Pawn(13,1,2, "white", false);
			whitepawns[3] = new Pawn(14,1,3, "white", false);
			whitepawns[4] = new Pawn(15,1,4, "white", false);
			whitepawns[5] = new Pawn(16,1,5, "white", false);
			whitepawns[6] = new Pawn(17,1,6, "white", false);
			whitepawns[7] = new Pawn(18,1,7, "white", false);
			
			PawnBlack []blackpawns = new PawnBlack[8];
			blackpawns[0] = new PawnBlack(21,6,0, "black", false);
			blackpawns[1] = new PawnBlack(22,6,1, "black", false);
			blackpawns[2] = new PawnBlack(23,6,2, "black", false);
			blackpawns[3] = new PawnBlack(24,6,3, "black", false);
			blackpawns[4] = new PawnBlack(25,6,4, "black", false);
			blackpawns[5] = new PawnBlack(26,6,5, "black", false);
			blackpawns[6] = new PawnBlack(27,6,6, "black", false);
			blackpawns[7] = new PawnBlack(28,6,7, "black", false);
			
			Rook rookLeftWhite = new Rook(31,0,0, "white", false);
			Rook rookRightWhite = new Rook(32,0,7, "white", false);
			RookBlack rookLeftBlack = new RookBlack(38,7,0, "black", false);
			RookBlack rookRightBlack = new RookBlack(39,7,7, "black", false);
			
			Knight knightLeftWhite = new Knight(41,0,1, "white", false);
			Knight knightRightWhite = new Knight(42,0,6, "white", false);
			KnightBlack knightLeftBlack = new KnightBlack(48,7,1, "black", false);
			KnightBlack knightRightBlack = new KnightBlack(49,7,6, "black", false);
			
			//checkforenemies doesnt work !!!
			Bishop bishopLeftWhite = new Bishop (51,0,2, "white",false);
			Bishop bishopRightWhite = new Bishop (52,0,5, "white",false);
			BishopBlack bishopLeftBlack = new BishopBlack (58,7,2, "black",false);
			BishopBlack bishopRightBlack = new BishopBlack (59,7,5, "black",false);
			
			Queen queenWhite = new Queen(61,0,3, "white", false);
			QueenBlack queenBlack = new QueenBlack(69,7,3, "black", false);
			
			King kingWhite = new King (71,0,4, "white", false);
			KingBlack kingBlack = new KingBlack(79,7,4,"black",false);
			
			int [] arrayWhite = new int[16];
			int [] arrayBlack = new int[16];
			
			for (int i=0; i<8; ++i) { //pawns
				arrayWhite[i]=i+11;
				arrayBlack[i]=i+21;
				//System.out.println("white: "+i +"  ->arr: "+arrayWhite[i]);
			}
			arrayWhite[8]=31; 	arrayWhite[9]=32;		arrayWhite[10]=41;	arrayWhite[11]=42;
			arrayWhite[12]=51; arrayWhite[13]=52; 		arrayWhite[14]=61; 	arrayWhite[15]=71;
			
			arrayBlack[8]=38; 	arrayBlack[9]=39;		arrayBlack[10]=48;	arrayBlack[11]=49;
			arrayBlack[12]=58; arrayBlack[13]=59; 		arrayBlack[14]=69; 	arrayBlack[15]=79;
		
			
	//deads : do while both kings are on the table, and memorize the kings index
	
		
	//SIMULATION
			
			/*whitepawns[4].move(matrix,visual);
			whitepawns[4].move(matrix,visual);
			whitepawns[4].move(matrix,visual);
			blackpawns[0].move(matrix,visual);
			blackpawns[0].move(matrix,visual);
			blackpawns[3].move(matrix,visual);
			blackpawns[4].move(matrix,visual);*/
			
		int randAgain=1;
		int ind;
		int dir;
		int nrofsteps;
		
	while ((isOnTable(visual,79)==true) && (isOnTable(visual,71)==true))
		{
			//STEP1 : whites
			

		while(randAgain>0) {
				ind = rand.nextInt(16);
				
				//ind = 13;
				 
				
			//ALL whitepawns	
			for (int i=0; i<8; i++)	{
			//0->11
				if (arrayWhite[ind] ==i+11) //
				{
					if (whitepawns[i].canmove(matrix))
					{
						randAgain=0; //no other move has to be generated
						whitepawns[i].move(matrix, visual);
					}
				}
			}
			
			//ALL whiteRooks :31,32
			
			if (arrayWhite[ind] ==31 ) { //rookLeftWhite
				if (rookLeftWhite.canmove(matrix)) {
					do { //generate valabil step
						dir = rand.nextInt(4);
						++dir; //correction
						
						if (dir ==1) {
							nrofsteps = rand.nextInt(5); //trick srry
							++nrofsteps; //correction
							if (isInRange(rookLeftWhite.x+nrofsteps))
								if(isWhite(visual[rookLeftWhite.x+nrofsteps][rookLeftWhite.y])==false){
									{
										rookLeftWhite.move(matrix,visual,nrofsteps,dir);
										randAgain=0; //step done
									}
								}
							
						}
						if (dir ==2) {
							nrofsteps = rand.nextInt(5);
							++nrofsteps; //correction
							if (isInRange(rookLeftWhite.x-nrofsteps))
								if(isWhite(visual[rookLeftWhite.x-nrofsteps][rookLeftWhite.y])==false){
									rookLeftWhite.move(matrix,visual,nrofsteps,dir);
									randAgain=0; //step done
								}
								
							
						}
						if (dir ==3) {
							nrofsteps = rand.nextInt(5);
							++nrofsteps; //correction
							if (isInRange(rookLeftWhite.y+nrofsteps))
								if(isWhite(visual[rookLeftWhite.y+nrofsteps][rookLeftWhite.y])==false)
								{
									rookLeftWhite.move(matrix,visual,nrofsteps,dir);
									randAgain=0; //step done
								}
						}
						if (dir ==4) {
							nrofsteps = rand.nextInt(5); 
							++nrofsteps; //correction
							if (isInRange(rookLeftWhite.y-nrofsteps))
								if(isWhite(visual[rookLeftWhite.y-nrofsteps][rookLeftWhite.y])==false)
								{
									rookLeftWhite.move(matrix,visual,nrofsteps,dir);
									randAgain=0; //step done
								}
						}
						
					}
					while(randAgain>0); //if valabil step found, randAgain is changed to 0
					
				}
			}//rookLeftWhite ends
			
			//rookRightWhite starts:
			
			if (arrayWhite[ind] ==32 ) { //rookRightWhite
				if (rookRightWhite.canmove(matrix)) {
					do { //generate valabil step
						dir = rand.nextInt(4);
						++dir; //correction
						
						if (dir ==1) {
							nrofsteps = rand.nextInt(5); //trick srry
							++nrofsteps; //correction
							if (isInRange(rookRightWhite.x+nrofsteps))
								if(isWhite(visual[rookRightWhite.x+nrofsteps][rookRightWhite.y])==false){
									{
										rookRightWhite.move(matrix,visual,nrofsteps,dir);
										randAgain=0; //step done
									}
								}
							
						}
						if (dir ==2) {
							nrofsteps = rand.nextInt(5);
							++nrofsteps; //correction
							if (isInRange(rookRightWhite.x-nrofsteps))
								if(isWhite(visual[rookRightWhite.x-nrofsteps][rookRightWhite.y])==false){
									rookRightWhite.move(matrix,visual,nrofsteps,dir);
									randAgain=0; //step done
								}
								
							
						}
						if (dir ==3) {
							nrofsteps = rand.nextInt(5);
							++nrofsteps; //correction
							if (isInRange(rookRightWhite.y+nrofsteps))
								if(isWhite(visual[rookRightWhite.y+nrofsteps][rookRightWhite.y])==false)
								{
									rookRightWhite.move(matrix,visual,nrofsteps,dir);
									randAgain=0; //step done
								}
						}
						if (dir ==4) {
							nrofsteps = rand.nextInt(5); 
							++nrofsteps; //correction
							if (isInRange(rookRightWhite.y-nrofsteps))
								if(isWhite(visual[rookRightWhite.y-nrofsteps][rookRightWhite.y])==false)
								{
									rookRightWhite.move(matrix,visual,nrofsteps,dir);
									randAgain=0; //step done
								}
						}
						
						
						
						
					}
					while(randAgain>0); //if valabil step found, randAgain is changed to 0
					
				}
			}//rookRightWhite ends
			
			//WHITE ROOKS END
				
			
				
				
				//ALL white knights:
				
				//knightLeftWhite:
				if (arrayWhite[ind] ==41 ) { //knightLeftWhite
					if (knightLeftWhite.canmove(matrix)) {
						do { //generate valabil step
							dir = rand.nextInt(8);
							++dir; //correction
							
							if (dir ==1) {
								//nrofsteps = rand.nextInt(5); //trick srry
								//++nrofsteps; //correction
								if (isInRange(knightLeftWhite.x+2))
									if (isInRange(knightLeftWhite.y+1))
										if(isWhite(visual[knightLeftWhite.x+2][knightLeftWhite.y+1])==false){
											{
												knightLeftWhite.move(matrix,visual,dir);
												randAgain=0; //step done
											}
										}
								
							}
							if (dir ==2) {
								//nrofsteps = rand.nextInt(5); //trick srry
								//++nrofsteps; //correction
								if (isInRange(knightLeftWhite.x+1))
									if (isInRange(knightLeftWhite.y+2))
										if(isWhite(visual[knightLeftWhite.x+1][knightLeftWhite.y+2])==false){
											{
												knightLeftWhite.move(matrix,visual,dir);
												randAgain=0; //step done
											}
										}
								
							}
							if (dir ==3) {
								//nrofsteps = rand.nextInt(5); //trick srry
								//++nrofsteps; //correction
								if (isInRange(knightLeftWhite.x+1))
									if (isInRange(knightLeftWhite.y-2))
										if(isWhite(visual[knightLeftWhite.x+1][knightLeftWhite.y-2])==false){
											{
												knightLeftWhite.move(matrix,visual,dir);
												randAgain=0; //step done
											}
										}
								
							}
							if (dir ==4) {
								//nrofsteps = rand.nextInt(5); //trick srry
								//++nrofsteps; //correction
								if (isInRange(knightLeftWhite.x-1))
									if (isInRange(knightLeftWhite.y+2))
										if(isWhite(visual[knightLeftWhite.x-1][knightLeftWhite.y+2])==false){
											{
												knightLeftWhite.move(matrix,visual,dir);
												randAgain=0; //step done
											}
										}
								
							}
							if (dir ==5) {
								//nrofsteps = rand.nextInt(5); //trick srry
								//++nrofsteps; //correction
								if (isInRange(knightLeftWhite.x-2))
									if (isInRange(knightLeftWhite.y+1))
										if(isWhite(visual[knightLeftWhite.x-2][knightLeftWhite.y+1])==false){
											{
												knightLeftWhite.move(matrix,visual,dir);
												randAgain=0; //step done
											}
										}
								
							}
							if (dir ==6) {
								//nrofsteps = rand.nextInt(5); //trick srry
								//++nrofsteps; //correction
								if (isInRange(knightLeftWhite.x+2))
									if (isInRange(knightLeftWhite.y-1))
										if(isWhite(visual[knightLeftWhite.x+2][knightLeftWhite.y-1])==false){
											{
												knightLeftWhite.move(matrix,visual,dir);
												randAgain=0; //step done
											}
										}
								
							}
							if (dir ==7) {
								//nrofsteps = rand.nextInt(5); //trick srry
								//++nrofsteps; //correction
								if (isInRange(knightLeftWhite.x-1))
									if (isInRange(knightLeftWhite.y-2))
										if(isWhite(visual[knightLeftWhite.x-1][knightLeftWhite.y-2])==false){
											{
												knightLeftWhite.move(matrix,visual,dir);
												randAgain=0; //step done
											}
										}
								
							}
							if (dir ==8) {
								//nrofsteps = rand.nextInt(5); //trick srry
								//++nrofsteps; //correction
								if (isInRange(knightLeftWhite.x-2))
									if (isInRange(knightLeftWhite.y-1))
										if(isWhite(visual[knightLeftWhite.x-2][knightLeftWhite.y-1])==false){
											{
												knightLeftWhite.move(matrix,visual,dir);
												randAgain=0; //step done
											}
										}
								
							}
						
						
								
							
						}
						while(randAgain>0); //if valabil step found, randAgain is changed to 0
						
					}
				
				
					
			}//arrayWhite check ends for knightLeftWhite
				
			// knightRightWhite starts:
				
				if (arrayWhite[ind] ==42 ) { //knightRightWhite
					if (knightRightWhite.canmove(matrix)) {
						do { //generate valabil step
							dir = rand.nextInt(8);
							++dir; //correction
							
							if (dir ==1) {
								//nrofsteps = rand.nextInt(5); //trick srry
								//++nrofsteps; //correction
								if (isInRange(knightRightWhite.x+2))
									if (isInRange(knightRightWhite.y+1))
										if(isWhite(visual[knightRightWhite.x+2][knightRightWhite.y+1])==false){
											{
												knightRightWhite.move(matrix,visual,dir);
												randAgain=0; //step done
											}
										}
								
							}
							if (dir ==2) {
								//nrofsteps = rand.nextInt(5); //trick srry
								//++nrofsteps; //correction
								if (isInRange(knightRightWhite.x+1))
									if (isInRange(knightRightWhite.y+2))
										if(isWhite(visual[knightRightWhite.x+1][knightRightWhite.y+2])==false){
											{
												knightRightWhite.move(matrix,visual,dir);
												randAgain=0; //step done
											}
										}
								
							}
							if (dir ==3) {
								//nrofsteps = rand.nextInt(5); //trick srry
								//++nrofsteps; //correction
								if (isInRange(knightRightWhite.x+1))
									if (isInRange(knightRightWhite.y-2))
										if(isWhite(visual[knightRightWhite.x+1][knightRightWhite.y-2])==false){
											{
												knightRightWhite.move(matrix,visual,dir);
												randAgain=0; //step done
											}
										}
								
							}
							if (dir ==4) {
								//nrofsteps = rand.nextInt(5); //trick srry
								//++nrofsteps; //correction
								if (isInRange(knightRightWhite.x-1))
									if (isInRange(knightRightWhite.y+2))
										if(isWhite(visual[knightRightWhite.x-1][knightRightWhite.y+2])==false){
											{
												knightRightWhite.move(matrix,visual,dir);
												randAgain=0; //step done
											}
										}
								
							}
							if (dir ==5) {
								//nrofsteps = rand.nextInt(5); //trick srry
								//++nrofsteps; //correction
								if (isInRange(knightRightWhite.x-2))
									if (isInRange(knightRightWhite.y+1))
										if(isWhite(visual[knightRightWhite.x-2][knightRightWhite.y+1])==false){
											{
												knightRightWhite.move(matrix,visual,dir);
												randAgain=0; //step done
											}
										}
								
							}
							if (dir ==6) {
								//nrofsteps = rand.nextInt(5); //trick srry
								//++nrofsteps; //correction
								if (isInRange(knightRightWhite.x+2))
									if (isInRange(knightRightWhite.y-1))
										if(isWhite(visual[knightRightWhite.x+2][knightRightWhite.y-1])==false){
											{
												knightRightWhite.move(matrix,visual,dir);
												randAgain=0; //step done
											}
										}
								
							}
							if (dir ==7) {
								//nrofsteps = rand.nextInt(5); //trick srry
								//++nrofsteps; //correction
								if (isInRange(knightRightWhite.x-1))
									if (isInRange(knightRightWhite.y-2))
										if(isWhite(visual[knightRightWhite.x-1][knightRightWhite.y-2])==false){
											{
												knightRightWhite.move(matrix,visual,dir);
												randAgain=0; //step done
											}
										}
								
							}
							if (dir ==8) {
								//nrofsteps = rand.nextInt(5); //trick srry
								//++nrofsteps; //correction
								if (isInRange(knightRightWhite.x-2))
									if (isInRange(knightRightWhite.y-1))
										if(isWhite(visual[knightRightWhite.x-2][knightRightWhite.y-1])==false){
											{
												knightRightWhite.move(matrix,visual,dir);
												randAgain=0; //step done
											}
										}
								
							}
								
							
						}
						while(randAgain>0); //if valabil step found, randAgain is changed to 0
						
					}
				
				
					
			}//arrayWhite check ends for knightRightWhite
				
				//WHITE KNIGHTS END
				
				
				
			//ALL WHITE Bishops:
			if (arrayWhite[ind]==51) { //bishopLeftWhite
				
				if (bishopLeftWhite.canmove(matrix)) {
					do { //generate valabil step
						dir = rand.nextInt(4);
						++dir; //correction
						
						if (dir ==1) {
							nrofsteps = rand.nextInt(5); //trick srry
							++nrofsteps; //correction
							if (isInRange(bishopLeftWhite.x+nrofsteps))
								if (isInRange(bishopLeftWhite.y+nrofsteps))
									if(isWhite(visual[bishopLeftWhite.x+nrofsteps][bishopLeftWhite.y+nrofsteps])==false){
										{
											bishopLeftWhite.move(matrix,visual,nrofsteps,dir);
											randAgain=0; //step done
										}
									}
							
						}
						if (dir ==2) {
							nrofsteps = rand.nextInt(5); //trick srry
							++nrofsteps; //correction
							if (isInRange(bishopLeftWhite.x-nrofsteps))
								if (isInRange(bishopLeftWhite.y+nrofsteps))
									if(isWhite(visual[bishopLeftWhite.x-nrofsteps][bishopLeftWhite.y+nrofsteps])==false){
										{
											bishopLeftWhite.move(matrix,visual,nrofsteps,dir);
											randAgain=0; //step done
										}
									}
							
						}
						if (dir ==3) {
							nrofsteps = rand.nextInt(5); //trick srry
							++nrofsteps; //correction
							if (isInRange(bishopLeftWhite.x+nrofsteps))
								if (isInRange(bishopLeftWhite.y-nrofsteps))
									if(isWhite(visual[bishopLeftWhite.x+nrofsteps][bishopLeftWhite.y-nrofsteps])==false){
										{
											bishopLeftWhite.move(matrix,visual,nrofsteps,dir);
											randAgain=0; //step done
										}
									}
							
						}
						if (dir ==4) {
							nrofsteps = rand.nextInt(5); //trick srry
							++nrofsteps; //correction
							if (isInRange(bishopLeftWhite.x-nrofsteps))
								if (isInRange(bishopLeftWhite.y-nrofsteps))
									if(isWhite(visual[bishopLeftWhite.x-nrofsteps][bishopLeftWhite.y-nrofsteps])==false){
										{
											bishopLeftWhite.move(matrix,visual,nrofsteps,dir);
											randAgain=0; //step done
										}
									}
							
						}
											
					}
					while(randAgain>0); //if valabil step found, randAgain is changed to 0
					
				}//if canmove
							
					
			} //arraycheck for bishopLeftWhite ends
			
			
			// bishopRightWhite starts: 
			if (arrayWhite[ind]==52) { //bishopRightWhite
				
				if (bishopRightWhite.canmove(matrix)) {
					do { //generate valabil step
						dir = rand.nextInt(4);
						++dir; //correction
						
						if (dir ==1) {
							nrofsteps = rand.nextInt(5); //trick srry
							++nrofsteps; //correction
							if (isInRange(bishopRightWhite.x+nrofsteps))
								if (isInRange(bishopRightWhite.y+nrofsteps))
									if(isWhite(visual[bishopRightWhite.x+nrofsteps][bishopRightWhite.y+nrofsteps])==false){
										{
											bishopRightWhite.move(matrix,visual,nrofsteps,dir);
											randAgain=0; //step done
										}
									}
							
						}
						if (dir ==2) {
							nrofsteps = rand.nextInt(5); //trick srry
							++nrofsteps; //correction
							if (isInRange(bishopRightWhite.x-nrofsteps))
								if (isInRange(bishopRightWhite.y+nrofsteps))
									if(isWhite(visual[bishopRightWhite.x-nrofsteps][bishopRightWhite.y+nrofsteps])==false){
										{
											bishopRightWhite.move(matrix,visual,nrofsteps,dir);
											randAgain=0; //step done
										}
									}
							
						}
						if (dir ==3) {
							nrofsteps = rand.nextInt(5); //trick srry
							++nrofsteps; //correction
							if (isInRange(bishopRightWhite.x+nrofsteps))
								if (isInRange(bishopRightWhite.y-nrofsteps))
									if(isWhite(visual[bishopRightWhite.x+nrofsteps][bishopRightWhite.y-nrofsteps])==false){
										{
											bishopRightWhite.move(matrix,visual,nrofsteps,dir);
											randAgain=0; //step done
										}
									}
						}
						if (dir ==4) {
							nrofsteps = rand.nextInt(5); //trick srry
							++nrofsteps; //correction
							if (isInRange(bishopRightWhite.x-nrofsteps))
								if (isInRange(bishopRightWhite.y-nrofsteps))
									if(isWhite(visual[bishopRightWhite.x-nrofsteps][bishopRightWhite.y-nrofsteps])==false){
										{
											bishopRightWhite.move(matrix,visual,nrofsteps,dir);
											randAgain=0; //step done
										}
									}
						}
						
					}
					while(randAgain>0); //if valabil step found, randAgain is changed to 0
					
				}//if canmove
				
					
			} //arraycheck for bishopRightWhite ends
			
			//ALL white BISHOPS end
			
			

			//ALL white QUEENS start:
			if (arrayWhite[ind] ==61 ) { //queenWhite
				if (queenWhite.canmove(matrix)) {
					do { //generate valabil step
						dir = rand.nextInt(8);
						++dir; //correction
						
						if (dir ==1) {
							nrofsteps = rand.nextInt(5); //trick srry
							++nrofsteps; //correction
							if (isInRange(queenWhite.x+nrofsteps))
								if (isInRange(queenWhite.y+nrofsteps))
									if(isWhite(visual[queenWhite.x+nrofsteps][queenWhite.y+nrofsteps])==false){
										{
											queenWhite.move(matrix,visual,nrofsteps,dir);
											randAgain=0; //step done
										}
									}
							
						}
						if (dir ==2) {
							nrofsteps = rand.nextInt(5); //trick srry
							++nrofsteps; //correction
							if (isInRange(queenWhite.x-nrofsteps))
								if (isInRange(queenWhite.y+nrofsteps))
									if(isWhite(visual[queenWhite.x-nrofsteps][queenWhite.y+nrofsteps])==false){
										{
											queenWhite.move(matrix,visual,nrofsteps,dir);
											randAgain=0; //step done
										}
									}
							
						}
						if (dir ==3) {
							nrofsteps = rand.nextInt(5); //trick srry
							++nrofsteps; //correction
							if (isInRange(queenWhite.x+nrofsteps))
								if (isInRange(queenWhite.y-nrofsteps))
									if(isWhite(visual[queenWhite.x+nrofsteps][queenWhite.y-nrofsteps])==false){
										{
											queenWhite.move(matrix,visual,nrofsteps,dir);
											randAgain=0; //step done
										}
									}
							
						}
						if (dir ==4) {
							nrofsteps = rand.nextInt(5); //trick srry
							++nrofsteps; //correction
							if (isInRange(queenWhite.x-nrofsteps))
								if (isInRange(queenWhite.y-nrofsteps))
									if(isWhite(visual[queenWhite.x-nrofsteps][queenWhite.y-nrofsteps])==false){
										{
											queenWhite.move(matrix,visual,nrofsteps,dir);
											randAgain=0; //step done
										}
									}
							
						}
						if (dir ==5) {
							nrofsteps = rand.nextInt(5); //trick srry
							++nrofsteps; //correction
							if (isInRange(queenWhite.x))
								if (isInRange(queenWhite.y-nrofsteps))
									if(isWhite(visual[queenWhite.x][queenWhite.y-nrofsteps])==false){
										{
											queenWhite.move(matrix,visual,nrofsteps,dir);
											randAgain=0; //step done
										}
									}
							
						}
						if (dir ==6) {
							nrofsteps = rand.nextInt(5); //trick srry
							++nrofsteps; //correction
							if (isInRange(queenWhite.x))
								if (isInRange(queenWhite.y+nrofsteps))
									if(isWhite(visual[queenWhite.x][queenWhite.y+nrofsteps])==false){
										{
											queenWhite.move(matrix,visual,nrofsteps,dir);
											randAgain=0; //step done
										}
									}
							
						}
						if (dir ==7) {
							nrofsteps = rand.nextInt(5); //trick srry
							++nrofsteps; //correction
							if (isInRange(queenWhite.x-nrofsteps))
								if (isInRange(queenWhite.y))
									if(isWhite(visual[queenWhite.x-nrofsteps][queenWhite.y])==false){
										{
											queenWhite.move(matrix,visual,nrofsteps,dir);
											randAgain=0; //step done
										}
									}
							
						}
						if (dir ==8) {
							nrofsteps = rand.nextInt(5); //trick srry
							++nrofsteps; //correction
							if (isInRange(queenWhite.x+nrofsteps))
								if (isInRange(queenWhite.y))
									if(isWhite(visual[queenWhite.x+nrofsteps][queenWhite.y])==false){
										{
											queenWhite.move(matrix,visual,nrofsteps,dir);
											randAgain=0; //step done
										}
									}
						}
					}
					while(randAgain>0); //if valabil step found, randAgain is changed to 0
					
				}
			
		}//arrayWhite check ends for queenWhite
			
			
		// ALL KING white start:
			
			if (arrayWhite[ind] ==71 ) { //kingWhite
				if (kingWhite.canmove(matrix)) {
					do { //generate valabil step
						dir = rand.nextInt(8);
						++dir; //correction
						
						if (dir ==1) {
							//nrofsteps = rand.nextInt(5); //trick srry
							//++nrofsteps; //correction
							if (isInRange(kingWhite.x+1))
								if (isInRange(kingWhite.y))
								if(isWhite(visual[kingWhite.x+1][kingWhite.y])==false){
									{
										kingWhite.move(matrix,visual,dir);
										randAgain=0; //step done
									}
								}
							
						}
						if (dir ==2) {
							//nrofsteps = rand.nextInt(5); //trick srry
							//++nrofsteps; //correction
							if (isInRange(kingWhite.x-1))
								if (isInRange(kingWhite.y))
								if(isWhite(visual[kingWhite.x-1][kingWhite.y])==false){
									{
										kingWhite.move(matrix,visual,dir);
										randAgain=0; //step done
									}
								}
							
						}
						if (dir ==3) {
							//nrofsteps = rand.nextInt(5); //trick srry
							//++nrofsteps; //correction
							if (isInRange(kingWhite.x))
								if (isInRange(kingWhite.y+1))
								if(isWhite(visual[kingWhite.x][kingWhite.y+1])==false){
									{
										kingWhite.move(matrix,visual,dir);
										randAgain=0; //step done
									}
								}
							
						}
						if (dir ==4) {
							//nrofsteps = rand.nextInt(5); //trick srry
							//++nrofsteps; //correction
							if (isInRange(kingWhite.x))
								if (isInRange(kingWhite.y-1))
								if(isWhite(visual[kingWhite.x][kingWhite.y-1])==false){
									{
										kingWhite.move(matrix,visual,dir);
										randAgain=0; //step done
									}
								}
							
						}
						if (dir ==5) {
							//nrofsteps = rand.nextInt(5); //trick srry
							//++nrofsteps; //correction
							if (isInRange(kingWhite.x+1))
								if (isInRange(kingWhite.y+1))
								if(isWhite(visual[kingWhite.x+1][kingWhite.y+1])==false){
									{
										kingWhite.move(matrix,visual,dir);
										randAgain=0; //step done
									}
								}
							
						}
						if (dir ==6) {
							//nrofsteps = rand.nextInt(5); //trick srry
							//++nrofsteps; //correction
							if (isInRange(kingWhite.x-1))
								if (isInRange(kingWhite.y+1))
								if(isWhite(visual[kingWhite.x-1][kingWhite.y+1])==false){
									{
										kingWhite.move(matrix,visual,dir);
										randAgain=0; //step done
									}
								}
							
						}
						if (dir ==7) {
							//nrofsteps = rand.nextInt(5); //trick srry
							//++nrofsteps; //correction
							if (isInRange(kingWhite.x+1))
								if (isInRange(kingWhite.y-1))
								if(isWhite(visual[kingWhite.x+1][kingWhite.y-1])==false){
									{
										kingWhite.move(matrix,visual,dir);
										randAgain=0; //step done
									}
								}
							
						}
						if (dir ==8) {
							//nrofsteps = rand.nextInt(5); //trick srry
							//++nrofsteps; //correction
							if (isInRange(kingWhite.x-1))
								if (isInRange(kingWhite.y-1))
								if(isWhite(visual[kingWhite.x-1][kingWhite.y-1])==false){
									{
										kingWhite.move(matrix,visual,dir);
										randAgain=0; //step done
									}
								}
							
						}
											
					}
					while(randAgain>0); //if valabil step found, randAgain is changed to 0
					
				}
				
				
			}	//kingwhite ends
			
			
			
			
			}//while randAgain(white) vege
		printVisual(visual);
		
		
		
		
		
		
		
		
		//BLACK FIGURES 
		//BLACK FIGURES
		//BLACK FIGURES
		randAgain = 1; //HERE starts BLACK figures
		while(randAgain>0) {
			ind = rand.nextInt(16);
			
			//ind = 14;
			 
			
		//ALL blackpawns	
		for (int i=0; i<8; i++)	{
		//0->11
			if (arrayBlack[ind] ==i+21) //
			{
				if (blackpawns[i].canmove(matrix))
				{
					randAgain=0; //no other move has to be generated
					blackpawns[i].move(matrix, visual);
				}
			}
		}
		//ALL blackRooks :38,39
		
		if (arrayBlack[ind] ==38 ) { //rookLeftBlack
			if (rookLeftBlack.canmove(matrix)) {
				do { //generate valabil step
					dir = rand.nextInt(4);
					++dir; //correction
					
					if (dir ==1) {
						nrofsteps = rand.nextInt(5); //trick srry
						++nrofsteps; //correction
						if (isInRange(rookLeftBlack.x+nrofsteps))
							if(isBlack(visual[rookLeftBlack.x+nrofsteps][rookLeftBlack.y])==false){
								{
									rookLeftBlack.move(matrix,visual,nrofsteps,dir);
									randAgain=0; //step done
								}
							}
						
					}
					if (dir ==2) {
						nrofsteps = rand.nextInt(5);
						++nrofsteps; //correction
						if (isInRange(rookLeftBlack.x-nrofsteps))
							if(isBlack(visual[rookLeftBlack.x-nrofsteps][rookLeftBlack.y])==false){
								rookLeftBlack.move(matrix,visual,nrofsteps,dir);
								randAgain=0; //step done
							}
							
						
					}
					if (dir ==3) {
						nrofsteps = rand.nextInt(5);
						++nrofsteps; //correction
						if (isInRange(rookLeftBlack.y+nrofsteps))
							if(isBlack(visual[rookLeftBlack.y+nrofsteps][rookLeftBlack.y])==false)
							{
								rookLeftBlack.move(matrix,visual,nrofsteps,dir);
								randAgain=0; //step done
							}
					}
					if (dir ==4) {
						nrofsteps = rand.nextInt(5); 
						++nrofsteps; //correction
						if (isInRange(rookLeftBlack.y-nrofsteps))
							if(isBlack(visual[rookLeftBlack.y-nrofsteps][rookLeftBlack.y])==false)
							{
								rookLeftBlack.move(matrix,visual,nrofsteps,dir);
								randAgain=0; //step done
							}
					}
					
				}
				while(randAgain>0); //if valabil step found, randAgain is changed to 0
				
			}
		}//rookLeftBlack ends
		
		//rookRightBlack starts:
	
		if (arrayBlack[ind] ==39 ) { //rookRightBlack
			System.out.println("I was there");
			
			if (rookRightBlack.canmove(matrix)) {
				do { //generate valabil step
					dir = rand.nextInt(4);
					++dir; //correction
					
					if (dir ==1) {
						nrofsteps = rand.nextInt(5); //trick srry
						++nrofsteps; //correction
						if (isInRange(rookRightBlack.x+nrofsteps))
							if(isBlack(visual[rookRightBlack.x+nrofsteps][rookRightBlack.y])==false){
								{
									rookRightBlack.move(matrix,visual,nrofsteps,dir);
									randAgain=0; //step done
								}
							}
						
					}
					if (dir ==2) {
						nrofsteps = rand.nextInt(5);
						++nrofsteps; //correction
						if (isInRange(rookRightBlack.x-nrofsteps))
							if(isBlack(visual[rookRightBlack.x-nrofsteps][rookRightBlack.y])==false){
								rookRightBlack.move(matrix,visual,nrofsteps,dir);
								randAgain=0; //step done
							}
							
						
					}
					if (dir ==3) {
						nrofsteps = rand.nextInt(5);
						++nrofsteps; //correction
						if (isInRange(rookRightBlack.y+nrofsteps))
							if(isBlack(visual[rookRightBlack.y+nrofsteps][rookRightBlack.y])==false)
							{
								rookRightBlack.move(matrix,visual,nrofsteps,dir);
								randAgain=0; //step done
							}
					}
					if (dir ==4) {
						nrofsteps = rand.nextInt(5); 
						++nrofsteps; //correction
						if (isInRange(rookRightBlack.y-nrofsteps))
							if(isBlack(visual[rookRightBlack.y-nrofsteps][rookRightBlack.y])==false)
							{
								rookRightBlack.move(matrix,visual,nrofsteps,dir);
								randAgain=0; //step done
							}
					}
					
					
					
					
				}
				while(randAgain>0); //if valabil step found, randAgain is changed to 0
				
			}
		}//rookrightBlack ends
		
		//Black ROOKS END
		
		
		//ALL BLACK knights:
		
		//knightLeftBlack:48,49
		if (arrayBlack[ind] ==48 ) { //knightLeftBlack
			if (knightLeftBlack.canmove(matrix)) {
				do { //generate valabil step
					dir = rand.nextInt(8);
					++dir; //correction
					
					if (dir ==1) {
						//nrofsteps = rand.nextInt(5); //trick srry
						//++nrofsteps; //correction
						if (isInRange(knightLeftBlack.x+2))
							if (isInRange(knightLeftBlack.y+1))
								if(isBlack(visual[knightLeftBlack.x+2][knightLeftBlack.y+1])==false){
									{
										knightLeftBlack.move(matrix,visual,dir);
										randAgain=0; //step done
									}
								}
						
					}
					if (dir ==2) {
						//nrofsteps = rand.nextInt(5); //trick srry
						//++nrofsteps; //correction
						if (isInRange(knightLeftBlack.x+1))
							if (isInRange(knightLeftBlack.y+2))
								if(isBlack(visual[knightLeftBlack.x+1][knightLeftBlack.y+2])==false){
									{
										knightLeftBlack.move(matrix,visual,dir);
										randAgain=0; //step done
									}
								}
						
					}
					if (dir ==3) {
						//nrofsteps = rand.nextInt(5); //trick srry
						//++nrofsteps; //correction
						if (isInRange(knightLeftBlack.x+1))
							if (isInRange(knightLeftBlack.y-2))
								if(isBlack(visual[knightLeftBlack.x+1][knightLeftBlack.y-2])==false){
									{
										knightLeftBlack.move(matrix,visual,dir);
										randAgain=0; //step done
									}
								}
						
					}
					if (dir ==4) {
						//nrofsteps = rand.nextInt(5); //trick srry
						//++nrofsteps; //correction
						if (isInRange(knightLeftBlack.x-1))
							if (isInRange(knightLeftBlack.y+2))
								if(isBlack(visual[knightLeftBlack.x-1][knightLeftBlack.y+2])==false){
									{
										knightLeftBlack.move(matrix,visual,dir);
										randAgain=0; //step done
									}
								}
						
					}
					if (dir ==5) {
						//nrofsteps = rand.nextInt(5); //trick srry
						//++nrofsteps; //correction
						if (isInRange(knightLeftBlack.x-2))
							if (isInRange(knightLeftBlack.y+1))
								if(isBlack(visual[knightLeftBlack.x-2][knightLeftBlack.y+1])==false){
									{
										knightLeftBlack.move(matrix,visual,dir);
										randAgain=0; //step done
									}
								}
						
					}
					if (dir ==6) {
						//nrofsteps = rand.nextInt(5); //trick srry
						//++nrofsteps; //correction
						if (isInRange(knightLeftBlack.x+2))
							if (isInRange(knightLeftBlack.y-1))
								if(isBlack(visual[knightLeftBlack.x+2][knightLeftBlack.y-1])==false){
									{
										knightLeftBlack.move(matrix,visual,dir);
										randAgain=0; //step done
									}
								}
						
					}
					if (dir ==7) {
						//nrofsteps = rand.nextInt(5); //trick srry
						//++nrofsteps; //correction
						if (isInRange(knightLeftBlack.x-1))
							if (isInRange(knightLeftBlack.y-2))
								if(isBlack(visual[knightLeftBlack.x-1][knightLeftBlack.y-2])==false){
									{
										knightLeftBlack.move(matrix,visual,dir);
										randAgain=0; //step done
									}
								}
						
					}
					if (dir ==8) {
						//nrofsteps = rand.nextInt(5); //trick srry
						//++nrofsteps; //correction
						if (isInRange(knightLeftBlack.x-2))
							if (isInRange(knightLeftBlack.y-1))
								if(isBlack(visual[knightLeftBlack.x-2][knightLeftBlack.y-1])==false){
									{
										knightLeftBlack.move(matrix,visual,dir);
										randAgain=0; //step done
									}
								}
						
					}
				
				
						
					
				}
				while(randAgain>0); //if valabil step found, randAgain is changed to 0
				
			}
		
		
			
	}//arrayBlack check ends for knightLeftBlack
		
	// knightRightBlack starts:
		
		if (arrayBlack[ind] ==49 ) { //knightRightWhite
			if (knightRightBlack.canmove(matrix)) {
				do { //generate valabil step
					dir = rand.nextInt(8);
					++dir; //correction
					
					if (dir ==1) {
						//nrofsteps = rand.nextInt(5); //trick srry
						//++nrofsteps; //correction
						if (isInRange(knightRightBlack.x+2))
							if (isInRange(knightRightBlack.y+1))
								if(isBlack(visual[knightRightBlack.x+2][knightRightBlack.y+1])==false){
									{
										knightRightBlack.move(matrix,visual,dir);
										randAgain=0; //step done
									}
								}
						
					}
					if (dir ==2) {
						//nrofsteps = rand.nextInt(5); //trick srry
						//++nrofsteps; //correction
						if (isInRange(knightRightBlack.x+1))
							if (isInRange(knightRightBlack.y+2))
								if(isBlack(visual[knightRightBlack.x+1][knightRightBlack.y+2])==false){
									{
										knightRightBlack.move(matrix,visual,dir);
										randAgain=0; //step done
									}
								}
						
					}
					if (dir ==3) {
						//nrofsteps = rand.nextInt(5); //trick srry
						//++nrofsteps; //correction
						if (isInRange(knightRightBlack.x+1))
							if (isInRange(knightRightBlack.y-2))
								if(isBlack(visual[knightRightBlack.x+1][knightRightBlack.y-2])==false){
									{
										knightRightBlack.move(matrix,visual,dir);
										randAgain=0; //step done
									}
								}
						
					}
					if (dir ==4) {
						//nrofsteps = rand.nextInt(5); //trick srry
						//++nrofsteps; //correction
						if (isInRange(knightRightBlack.x-1))
							if (isInRange(knightRightBlack.y+2))
								if(isBlack(visual[knightRightBlack.x-1][knightRightBlack.y+2])==false){
									{
										knightRightBlack.move(matrix,visual,dir);
										randAgain=0; //step done
									}
								}
						
					}
					if (dir ==5) {
						//nrofsteps = rand.nextInt(5); //trick srry
						//++nrofsteps; //correction
						if (isInRange(knightRightBlack.x-2))
							if (isInRange(knightRightBlack.y+1))
								if(isBlack(visual[knightRightBlack.x-2][knightRightBlack.y+1])==false){
									{
										knightRightBlack.move(matrix,visual,dir);
										randAgain=0; //step done
									}
								}
						
					}
					if (dir ==6) {
						//nrofsteps = rand.nextInt(5); //trick srry
						//++nrofsteps; //correction
						if (isInRange(knightRightBlack.x+2))
							if (isInRange(knightRightBlack.y-1))
								if(isBlack(visual[knightRightBlack.x+2][knightRightBlack.y-1])==false){
									{
										knightRightBlack.move(matrix,visual,dir);
										randAgain=0; //step done
									}
								}
						
					}
					if (dir ==7) {
						//nrofsteps = rand.nextInt(5); //trick srry
						//++nrofsteps; //correction
						if (isInRange(knightRightBlack.x-1))
							if (isInRange(knightRightBlack.y-2))
								if(isBlack(visual[knightRightBlack.x-1][knightRightBlack.y-2])==false){
									{
										knightRightBlack.move(matrix,visual,dir);
										randAgain=0; //step done
									}
								}
						
					}
					if (dir ==8) {
						//nrofsteps = rand.nextInt(5); //trick srry
						//++nrofsteps; //correction
						if (isInRange(knightRightBlack.x-2))
							if (isInRange(knightRightBlack.y-1))
								if(isBlack(visual[knightRightBlack.x-2][knightRightBlack.y-1])==false){
									{
										knightRightBlack.move(matrix,visual,dir);
										randAgain=0; //step done
									}
								}
						
					}
						
					
				}
				while(randAgain>0); //if valabil step found, randAgain is changed to 0
				
			}
		
		
			
	}//arrayBLACK check ends for knightRightBlack
		
		//BLACK KNIGHTS END
		
		
		
		//ALL BLACK Bishops:
		if (arrayBlack[ind]==58) { //bishopLeftBlack
			
			if (bishopLeftBlack.canmove(matrix)) {
				do { //generate valabil step
					dir = rand.nextInt(4);
					++dir; //correction
					
					if (dir ==1) {
						nrofsteps = rand.nextInt(5); //trick srry
						++nrofsteps; //correction
						if (isInRange(bishopLeftBlack.x+nrofsteps))
							if (isInRange(bishopLeftBlack.y+nrofsteps))
								if(isBlack(visual[bishopLeftBlack.x+nrofsteps][bishopLeftBlack.y+nrofsteps])==false){
									{
										bishopLeftBlack.move(matrix,visual,nrofsteps,dir);
										randAgain=0; //step done
									}
								}
						
					}
					if (dir ==2) {
						nrofsteps = rand.nextInt(5); //trick srry
						++nrofsteps; //correction
						if (isInRange(bishopLeftBlack.x-nrofsteps))
							if (isInRange(bishopLeftBlack.y+nrofsteps))
								if(isBlack(visual[bishopLeftBlack.x-nrofsteps][bishopLeftBlack.y+nrofsteps])==false){
									{
										bishopLeftBlack.move(matrix,visual,nrofsteps,dir);
										randAgain=0; //step done
									}
								}
						
					}
					if (dir ==3) {
						nrofsteps = rand.nextInt(5); //trick srry
						++nrofsteps; //correction
						if (isInRange(bishopLeftBlack.x+nrofsteps))
							if (isInRange(bishopLeftBlack.y-nrofsteps))
								if(isBlack(visual[bishopLeftBlack.x+nrofsteps][bishopLeftBlack.y-nrofsteps])==false){
									{
										bishopLeftBlack.move(matrix,visual,nrofsteps,dir);
										randAgain=0; //step done
									}
								}
						
					}
					if (dir ==4) {
						nrofsteps = rand.nextInt(5); //trick srry
						++nrofsteps; //correction
						if (isInRange(bishopLeftBlack.x-nrofsteps))
							if (isInRange(bishopLeftBlack.y-nrofsteps))
								if(isBlack(visual[bishopLeftBlack.x-nrofsteps][bishopLeftBlack.y-nrofsteps])==false){
									{
										bishopLeftBlack.move(matrix,visual,nrofsteps,dir);
										randAgain=0; //step done
									}
								}
						
					}
										
				}
				while(randAgain>0); //if valabil step found, randAgain is changed to 0
				
			}//if canmove
						
				
		} //arraycheck for bishopLeftBlack ends
		
		
		// bishopRightBlack starts: 
		if (arrayWhite[ind]==59) { //bishopRightBlack
			
			if (bishopRightBlack.canmove(matrix)) {
				do { //generate valabil step
					dir = rand.nextInt(4);
					++dir; //correction
					
					if (dir ==1) {
						nrofsteps = rand.nextInt(5); //trick srry
						++nrofsteps; //correction
						if (isInRange(bishopRightBlack.x+nrofsteps))
							if (isInRange(bishopRightBlack.y+nrofsteps))
								if(isBlack(visual[bishopRightBlack.x+nrofsteps][bishopRightBlack.y+nrofsteps])==false){
									{
										bishopRightBlack.move(matrix,visual,nrofsteps,dir);
										randAgain=0; //step done
									}
								}
						
					}
					if (dir ==2) {
						nrofsteps = rand.nextInt(5); //trick srry
						++nrofsteps; //correction
						if (isInRange(bishopRightBlack.x-nrofsteps))
							if (isInRange(bishopRightBlack.y+nrofsteps))
								if(isBlack(visual[bishopRightBlack.x-nrofsteps][bishopRightBlack.y+nrofsteps])==false){
									{
										bishopRightBlack.move(matrix,visual,nrofsteps,dir);
										randAgain=0; //step done
									}
								}
						
					}
					if (dir ==3) {
						nrofsteps = rand.nextInt(5); //trick srry
						++nrofsteps; //correction
						if (isInRange(bishopRightBlack.x+nrofsteps))
							if (isInRange(bishopRightBlack.y-nrofsteps))
								if(isBlack(visual[bishopRightBlack.x+nrofsteps][bishopRightBlack.y-nrofsteps])==false){
									{
										bishopRightBlack.move(matrix,visual,nrofsteps,dir);
										randAgain=0; //step done
									}
								}
					}
					if (dir ==4) {
						nrofsteps = rand.nextInt(5); //trick srry
						++nrofsteps; //correction
						if (isInRange(bishopRightBlack.x-nrofsteps))
							if (isInRange(bishopRightBlack.y-nrofsteps))
								if(isBlack(visual[bishopRightBlack.x-nrofsteps][bishopRightBlack.y-nrofsteps])==false){
									{
										bishopRightBlack.move(matrix,visual,nrofsteps,dir);
										randAgain=0; //step done
									}
								}
					}
					
				}
				while(randAgain>0); //if valabil step found, randAgain is changed to 0
				
			}//if canmove
			
				
		} //arraycheck for bishopRightBlack ends
		
		//ALL black BISHOPS end
		
		
		//ALL black QUEENS start:
		
		if (arrayBlack[ind] ==69 ) { //queenBlack
			if (queenBlack.canmove(matrix)) {
				do { //generate valabil step
					dir = rand.nextInt(8);
					++dir; //correction
					
					if (dir ==1) {
						nrofsteps = rand.nextInt(3); //trick srry
						++nrofsteps; //correction
						if (isInRange(queenBlack.x+nrofsteps))
							if (isInRange(queenBlack.y+nrofsteps))
								if(isBlack(visual[queenBlack.x+nrofsteps][queenBlack.y+nrofsteps])==false){
									{
										queenBlack.move(matrix,visual,nrofsteps,dir);
										randAgain=0; //step done
									}
								}
						
					}
					if (dir ==2) {
						nrofsteps = rand.nextInt(3); //trick srry
						++nrofsteps; //correction
						if (isInRange(queenBlack.x-nrofsteps))
							if (isInRange(queenBlack.y+nrofsteps))
								if(isBlack(visual[queenBlack.x-nrofsteps][queenBlack.y+nrofsteps])==false){
									{
										queenBlack.move(matrix,visual,nrofsteps,dir);
										randAgain=0; //step done
									}
								}
						
					}
					if (dir ==3) {
						nrofsteps = rand.nextInt(3); //trick srry
						++nrofsteps; //correction
						if (isInRange(queenBlack.x+nrofsteps))
							if (isInRange(queenBlack.y-nrofsteps))
								if(isBlack(visual[queenBlack.x+nrofsteps][queenBlack.y-nrofsteps])==false){
									{
										queenBlack.move(matrix,visual,nrofsteps,dir);
										randAgain=0; //step done
									}
								}
						
					}
					if (dir ==4) {
						nrofsteps = rand.nextInt(3); //trick srry
						++nrofsteps; //correction
						if (isInRange(queenBlack.x-nrofsteps))
							if (isInRange(queenBlack.y-nrofsteps))
								if(isBlack(visual[queenBlack.x-nrofsteps][queenBlack.y-nrofsteps])==false){
									{
										queenBlack.move(matrix,visual,nrofsteps,dir);
										randAgain=0; //step done
									}
								}
						
					}
					if (dir ==5) {
						nrofsteps = rand.nextInt(3); //trick srry
						++nrofsteps; //correction
						if (isInRange(queenBlack.x))
							if (isInRange(queenBlack.y-nrofsteps))
								if(isBlack(visual[queenBlack.x][queenBlack.y-nrofsteps])==false){
									{
										queenBlack.move(matrix,visual,nrofsteps,dir);
										randAgain=0; //step done
									}
								}
						
					}
					if (dir ==6) {
						nrofsteps = rand.nextInt(3); //trick srry
						++nrofsteps; //correction
						if (isInRange(queenBlack.x))
							if (isInRange(queenBlack.y+nrofsteps))
								if(isBlack(visual[queenBlack.x][queenBlack.y+nrofsteps])==false){
									{
										queenBlack.move(matrix,visual,nrofsteps,dir);
										randAgain=0; //step done
									}
								}
						
					}
					if (dir ==7) {
						nrofsteps = rand.nextInt(3); //trick srry
						++nrofsteps; //correction
						if (isInRange(queenBlack.x-nrofsteps))
							if (isInRange(queenBlack.y))
								if(isBlack(visual[queenBlack.x-nrofsteps][queenBlack.y])==false){
									{
										queenBlack.move(matrix,visual,nrofsteps,dir);
										randAgain=0; //step done
									}
								}
						
					}
					
					//SOME ERROR HERE, i dont understand why
					if (dir ==8) {
						nrofsteps = rand.nextInt(3); //trick srry
						++nrofsteps; //correction
						if (isInRange(queenBlack.x+nrofsteps))
							if (isInRange(queenBlack.y))
								if(isBlack(visual[queenBlack.x+nrofsteps][queenBlack.y])==false){
									{
										queenBlack.move(matrix,visual,nrofsteps,dir);
										randAgain=0; //step done
									}
								}
					}
				}
				while(randAgain>0); //if valabil step found, randAgain is changed to 0
				
			}
		
	}//arrayBlack check ends for queenBlack
		
		//ALL QUEEN black ends
		
		
		// ALL KING black starts:
		
		if (arrayBlack[ind] ==79 ) { //kingBlack
			if (kingBlack.canmove(matrix)) {
				do { //generate valabil step
					dir = rand.nextInt(8);
					++dir; //correction
					
					if (dir ==1) {
						//nrofsteps = rand.nextInt(5); //trick srry
						//++nrofsteps; //correction
						if (isInRange(kingBlack.x+1))
							if (isInRange(kingBlack.y))
							if(isBlack(visual[kingBlack.x+1][kingBlack.y])==false){
								{
									kingBlack.move(matrix,visual,dir);
									randAgain=0; //step done
								}
							}
						
					}
					if (dir ==2) {
						//nrofsteps = rand.nextInt(5); //trick srry
						//++nrofsteps; //correction
						if (isInRange(kingBlack.x-1))
							if (isInRange(kingBlack.y))
							if(isBlack(visual[kingBlack.x-1][kingBlack.y])==false){
								{
									kingBlack.move(matrix,visual,dir);
									randAgain=0; //step done
								}
							}
						
					}
					if (dir ==3) {
						//nrofsteps = rand.nextInt(5); //trick srry
						//++nrofsteps; //correction
						if (isInRange(kingBlack.x))
							if (isInRange(kingBlack.y+1))
							if(isBlack(visual[kingBlack.x][kingBlack.y+1])==false){
								{
									kingBlack.move(matrix,visual,dir);
									randAgain=0; //step done
								}
							}
						
					}
					if (dir ==4) {
						//nrofsteps = rand.nextInt(5); //trick srry
						//++nrofsteps; //correction
						if (isInRange(kingBlack.x))
							if (isInRange(kingBlack.y-1))
							if(isBlack(visual[kingBlack.x][kingBlack.y-1])==false){
								{
									kingBlack.move(matrix,visual,dir);
									randAgain=0; //step done
								}
							}
						
					}
					if (dir ==5) {
						//nrofsteps = rand.nextInt(5); //trick srry
						//++nrofsteps; //correction
						if (isInRange(kingBlack.x+1))
							if (isInRange(kingBlack.y+1))
							if(isBlack(visual[kingBlack.x+1][kingBlack.y+1])==false){
								{
									kingBlack.move(matrix,visual,dir);
									randAgain=0; //step done
								}
							}
						
					}
					if (dir ==6) {
						//nrofsteps = rand.nextInt(5); //trick srry
						//++nrofsteps; //correction
						if (isInRange(kingBlack.x-1))
							if (isInRange(kingBlack.y+1))
							if(isBlack(visual[kingBlack.x-1][kingBlack.y+1])==false){
								{
									kingBlack.move(matrix,visual,dir);
									randAgain=0; //step done
								}
							}
						
					}
					if (dir ==7) {
						//nrofsteps = rand.nextInt(5); //trick srry
						//++nrofsteps; //correction
						if (isInRange(kingBlack.x+1))
							if (isInRange(kingBlack.y-1))
							if(isBlack(visual[kingBlack.x+1][kingBlack.y-1])==false){
								{
									kingBlack.move(matrix,visual,dir);
									randAgain=0; //step done
								}
							}
						
					}
					if (dir ==8) {
						//nrofsteps = rand.nextInt(5); //trick srry
						//++nrofsteps; //correction
						if (isInRange(kingBlack.x-1))
							if (isInRange(kingBlack.y-1))
							if(isBlack(visual[kingBlack.x-1][kingBlack.y-1])==false){
								{
									kingBlack.move(matrix,visual,dir);
									randAgain=0; //step done
								}
							}
						
					}
										
				}
				while(randAgain>0); //if valabil step found, randAgain is changed to 0
				
			}
			
			
		}	//kingBlack ends
		
		
		
		
		
		
		
		
		
		
		
		
		
	
		
			
		}//while (randAgain) for blacks ends
		printVisual(visual);	
	
			
	
		
		
		
		
		
	randAgain = 1; //force next move for whites		
	} //while (king) vege
			
		System.out.println("");
		System.out.println("");
		System.out.println("");
		System.out.println("In the end:");
		if (isOnTable(visual,79))
			System.out.println("Black WON");
		else
			System.out.println("White WON");
		

		printVisual(visual);
		printBoolMatrix(matrix);
		
		

}	
	
	}


